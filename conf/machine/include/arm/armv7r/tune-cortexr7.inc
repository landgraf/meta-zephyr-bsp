#
# Tune Settings for Cortex-R7
#
DEFAULTTUNE ?= "cortexr7"

TUNEVALID[cortexr7] = "Enable Cortex-R7 specific processor optimizations"
TUNE_CCARGS .= "${@bb.utils.contains('TUNE_FEATURES', 'cortexr7', ' -mcpu=cortex-r7', '', d)}"

require conf/machine/include/arm/arch-armv7r.inc

AVAILTUNES                            += "cortexr7"
ARMPKGARCH_tune-cortexr7               = "cortexr7"
TUNE_FEATURES_tune-cortexr7            = "${TUNE_FEATURES_tune-armv7r-vfpv3d16} cortexr7 idiv"
PACKAGE_EXTRA_ARCHS_tune-cortexr7      = "${PACKAGE_EXTRA_ARCHS_tune-armv7r-vfpv3d16} cortexr7-vfpv3d16"
