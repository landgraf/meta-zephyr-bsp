LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=fa818a259cbed7ce8bc2a22d35a464fc"

inherit cmake

# This file might be included from other places (like other layers) and not
# having an explicit path to the patches directory, will make bitbake fail to
# find the patch(es) in SRC_URI.
FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI = "\
    git://github.com/zephyrproject-rtos/zephyr.git;protocol=https;branch=${ZEPHYR_BRANCH};name=default \
    git://github.com/zephyrproject-rtos/cmsis.git;protocol=https;destsuffix=git/modules/cmsis;name=cmsis \
    git://github.com/zephyrproject-rtos/hal_nordic.git;protocol=https;destsuffix=git/modules/hal/nordic;name=nordic \
    git://github.com/zephyrproject-rtos/hal_stm32.git;protocol=https;destsuffix=git/modules/hal/stm32;name=stm32 \
    git://github.com/zephyrproject-rtos/hal_atmel.git;protocol=https;branch=master;destsuffix=git/modules/hal/atmel;name=hal_atmel \ 
    git://github.com/zephyrproject-rtos/hal_altera.git;protocol=https;branch=master;destsuffix=git/modules/hal/altera;name=hal_altera \ 
    git://github.com/zephyrproject-rtos/canopennode.git;protocol=https;branch=zephyr;destsuffix=git/modules/lib/canopennode;name=canopennode \ 
    git://github.com/zephyrproject-rtos/civetweb.git;protocol=https;branch=zephyr;destsuffix=git/modules/lib/civetweb;name=civetweb \ 
    git://github.com/zephyrproject-rtos/hal_espressif.git;protocol=https;branch=hal_v4.2;destsuffix=git/modules/hal/espressif;name=hal_espressif \ 
    git://github.com/zephyrproject-rtos/fatfs.git;protocol=https;branch=master;destsuffix=git/modules/fs/fatfs;name=fatfs \ 
    git://github.com/zephyrproject-rtos/hal_cypress.git;protocol=https;branch=master;destsuffix=git/modules/hal/cypress;name=hal_cypress \ 
    git://github.com/zephyrproject-rtos/hal_infineon.git;protocol=https;branch=master;destsuffix=git/modules/hal/infineon;name=hal_infineon \ 
    git://github.com/zephyrproject-rtos/hal_nordic.git;protocol=https;branch=master;destsuffix=git/modules/hal/nordic;name=hal_nordic \ 
    git://github.com/zephyrproject-rtos/hal_openisa.git;protocol=https;branch=master;destsuffix=git/modules/hal/openisa;name=hal_openisa \ 
    git://github.com/zephyrproject-rtos/hal_nuvoton.git;protocol=https;branch=master;destsuffix=git/modules/hal/nuvoton;name=hal_nuvoton \ 
    git://github.com/zephyrproject-rtos/hal_microchip.git;protocol=https;branch=master;destsuffix=git/modules/hal/microchip;name=hal_microchip \ 
    git://github.com/zephyrproject-rtos/hal_silabs.git;protocol=https;branch=master;destsuffix=git/modules/hal/silabs;name=hal_silabs \ 
    git://github.com/zephyrproject-rtos/hal_st.git;protocol=https;branch=master;destsuffix=git/modules/hal/st;name=hal_st \ 
    git://github.com/zephyrproject-rtos/hal_stm32.git;protocol=https;branch=master;destsuffix=git/modules/hal/stm32;name=hal_stm32 \ 
    git://github.com/zephyrproject-rtos/hal_ti.git;protocol=https;branch=master;destsuffix=git/modules/hal/ti;name=hal_ti \ 
    git://github.com/zephyrproject-rtos/libmetal.git;protocol=https;branch=master;destsuffix=git/modules/hal/libmetal;name=libmetal \ 
    git://github.com/zephyrproject-rtos/hal_quicklogic.git;protocol=https;branch=master;destsuffix=git/modules/hal/quicklogic;name=hal_quicklogic \ 
    git://github.com/zephyrproject-rtos/lvgl.git;protocol=https;branch=zephyr;destsuffix=git/modules/lib/gui/lvgl;name=lvgl \ 
    git://github.com/zephyrproject-rtos/mbedtls.git;protocol=https;branch=master;destsuffix=git/modules/crypto/mbedtls;name=mbedtls \ 
    git://github.com/zephyrproject-rtos/mcuboot.git;protocol=https;branch=main;destsuffix=git/bootloader/mcuboot;name=mcuboot \ 
    git://github.com/zephyrproject-rtos/mcumgr.git;protocol=https;destsuffix=git/modules/lib/mcumgr;name=mcumgr \ 
    git://github.com/zephyrproject-rtos/net-tools.git;protocol=https;destsuffix=git/tools/net-tools;name=net-tools \ 
    git://github.com/zephyrproject-rtos/hal_nxp.git;protocol=https;destsuffix=git/modules/hal/nxp;name=hal_nxp \ 
    git://github.com/zephyrproject-rtos/open-amp.git;protocol=https;destsuffix=git/modules/lib/open-amp;name=open-amp \ 
    git://github.com/zephyrproject-rtos/loramac-node.git;protocol=https;branch=zephyr-v2.6;destsuffix=git/modules/lib/loramac-node;name=loramac-node \ 
    git://github.com/zephyrproject-rtos/openthread.git;protocol=https;branch=zephyr;destsuffix=git/modules/lib/openthread;name=openthread \ 
    git://github.com/zephyrproject-rtos/segger.git;protocol=https;destsuffix=git/modules/debug/segger;name=segger \ 
    git://github.com/zephyrproject-rtos/sof.git;branch=zephyr_2.6;protocol=https;destsuffix=git/modules/audio/sof;name=sof \ 
    git://github.com/zephyrproject-rtos/tinycbor.git;branch=zephyr;protocol=https;destsuffix=git/modules/lib/tinycbor;name=tinycbor \ 
    git://github.com/zephyrproject-rtos/tinycrypt.git;protocol=https;destsuffix=git/modules/crypto/tinycrypt;name=tinycrypt \ 
    git://github.com/zephyrproject-rtos/littlefs.git;branch=zephyr;protocol=https;destsuffix=git/modules/fs/littlefs;name=littlefs \ 
    git://github.com/zephyrproject-rtos/mipi-sys-t.git;branch=zephyr;protocol=https;destsuffix=git/modules/debug/mipi-sys-t;name=mipi-sys-t \ 
    git://github.com/zephyrproject-rtos/nrf_hw_models.git;protocol=https;destsuffix=git/modules/bsim_hw_models/nrf_hw_models;name=nrf_hw_models \ 
    git://github.com/zephyrproject-rtos/TraceRecorderSource.git;branch=zephyr;protocol=https;destsuffix=git/modules/debug/TraceRecorder;name=TraceRecorderSource \ 
    git://github.com/zephyrproject-rtos/hal_xtensa.git;branch=master;protocol=https;destsuffix=git/modules/hal/xtensa;name=hal_xtensa \ 
    git://github.com/zephyrproject-rtos/edtt.git;protocol=https;branch=public_master;destsuffix=git/tools/edtt;name=edtt \ 
    git://github.com/zephyrproject-rtos/trusted-firmware-m.git;protocol=https;branch=legacy;destsuffix=git/modules/tee/tfm;name=trusted-firmware-m \ 
    git://github.com/zephyrproject-rtos/nanopb.git;branch=zephyr;protocol=https;destsuffix=git/modules/lib/nanopb;name=nanopb \ 
    git://github.com/zephyrproject-rtos/tensorflow.git;branch=zephyr;protocol=https;destsuffix=git/modules/lib/tensorflow;name=tensorflow \ 
    file://arduino-usb-cdc-acm-enble.patch \
    "
S = "${WORKDIR}/git"

# Default to a stable version
PREFERRED_VERSION_zephyr-kernel ??= "2.6.0"
include zephyr-kernel-src-${PREFERRED_VERSION_zephyr-kernel}.inc
