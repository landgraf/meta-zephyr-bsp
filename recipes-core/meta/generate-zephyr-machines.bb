# SPDX-FileCopyrightText: Huawei Inc.
# SPDX-License-Identifier: Apache-2.0

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

require recipes-kernel/zephyr-kernel/zephyr-sample.inc

ZEPHYR_SRC_DIR = "${S}/samples/hello_world"

OECMAKE_SOURCEPATH = "${ZEPHYR_SRC_DIR}"

OECMAKE_GENERATOR_ARGS += "-c"

EXTRA_OECMAKE += "\
                -DCONFIG_OEMACHINE_EXPORTS=y \
                -DMETA_OE_BASE:STRING=\"${ZEPHYR_BSP_TOPDIR}\" \
                "

SRC_URI_append = "file://0001-zephyr-Export-an-OpenEmbedded-machine-config.patch"

do_deploy () {
    install -D ${B}/${MACHINE}.conf ${DEPLOYDIR}/${MACHINE}.conf
}
